import React from 'react'
import { formValueSelector } from 'redux-form'
import { connect } from 'react-redux'
import { Table } from 'reactstrap';

let ShowPostSelector = (props) =>
  <div>
    <h2>Preview</h2>
    <Table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Gender</th>
          <th>Email</th>
          <th>Mobile</th>
          <th>Technology</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td>{JSON.stringify(props.Name)}</td>
          <td>{JSON.stringify(props.Gender)}</td>
          <td>{JSON.stringify(props.Email)}</td>
          <td>{JSON.stringify(props.phone)}</td>
          <td>{JSON.stringify(props.checkbox)}</td>
        </tr>
      </tbody>
    </Table>
  </div>

const mapStateToProps = state => ({
  Name: formValueSelector("simpleForm")(state, "name"),
  Gender: formValueSelector("simpleForm")(state, "gender"),
  Email: formValueSelector("simpleForm")(state, "email"),
  phone: formValueSelector("simpleForm")(state, "phoneNo"),
  file: formValueSelector("simpleForm")(state, "file"),
  checkbox: {C: formValueSelector("simpleForm")(state, "C"),
    Cpp : formValueSelector("simpleForm")(state, "C++"),
    Java : formValueSelector("simpleForm")(state, "Java"),
    Python : formValueSelector("simpleForm")(state, "Python"),
    Javascript : formValueSelector("simpleForm")(state, "Javascript")
  }
})

ShowPostSelector = connect(
  mapStateToProps
)(ShowPostSelector)

export default ShowPostSelector