import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import {Field , reduxForm} from 'redux-form';
import FieldFileInput from "./FieldFileInput";

let Example = (props) => {
  const { handleSubmit, pristine, reset, submitting } = props
    return (
      <div style={{display:"flex",height:'100vh',textAlign:'center',justifyContent:'center',alignItems:'center'}}>
      <Form onSubmit={handleSubmit}>
        <FormGroup>
          <Label for="Name">Name</Label>
          <Field component="input" type="text" name="name" id="name" placeholder="Enter Name" 
          pattern="[A-Za-z]{2,30}" />
        </FormGroup>
        
        <FormGroup tag="fieldset">
          <legend>Radio Buttons</legend>
          <FormGroup check>
            <Label check for="radio1-option1">
            <Field component="input" type="radio" value="Male" name="gender" />{' '}
              Male
            </Label>
            </FormGroup>
            <FormGroup check>
            <Label check for="radio1-option2">
            <Field component="input" type="radio" value="Female" name="gender" />{' '}
              Female
            </Label>
            </FormGroup>
            <FormGroup check>
            <Label check for="radio1-option2">
            <Field component="input" type="radio" value="Others" name="gender" />{' '}
              Others
            </Label>
            </FormGroup>
        </FormGroup>

        <FormGroup>
          <Label for="Email">Email</Label>
          <Field component="input" type="email" name="email" id="email" placeholder="Enter email" />
        </FormGroup>
        
        <FormGroup>
          <Label for="phoneNo">Mobile Number</Label>
          <Field component="input" type="tele" name="phoneNo" id="phoneNo" placeholder="Enter Mobile Number" pattern="[1-9]{1}[0-9]{9}"/>
        </FormGroup>
        
        <FormGroup>
          <Label for="exampleFile">Profile Picture</Label>
          <FieldFileInput />
          <FormText color="muted">
            Choose the Profile Picture (Only JPEG, JPG and PNG file types are allowed)
          </FormText>
        </FormGroup>
        
        
        <FormGroup>
            <legend for="Technology"> Technology </legend>
            
            <FormGroup check inline>
                <Field component="input" id="InlineCheckboxes-checkbox-1" name="C" type="checkbox" />
                <Label for="InlineCheckboxes-checkbox-1" check>
                    C
                </Label>
            </FormGroup>
            <FormGroup check inline>
                <Field component="input" id="InlineCheckboxes-checkbox-2" name="C++" type="checkbox" />
                <Label for="InlineCheckboxes-checkbox-2" check>
                    C++
                </Label>
            </FormGroup>
            <FormGroup check inline>
                <Field component="input" id="InlineCheckboxes-checkbox-2" name="Java" type="checkbox" />
                <Label for="InlineCheckboxes-checkbox-2" check>
                    Java
                </Label>
            </FormGroup>
            <FormGroup check inline>
                <Field component="input" id="InlineCheckboxes-checkbox-2" name="Python" type="checkbox" />
                <Label for="InlineCheckboxes-checkbox-2" check>
                    Python
                </Label>
            </FormGroup>
            <FormGroup check inline>
                <Field component="input" id="InlineCheckboxes-checkbox-2" name="Javascript" type="checkbox" />
                <Label for="InlineCheckboxes-checkbox-2" check>
                    Javascript
                </Label>
            </FormGroup>
        </FormGroup>
        <Button type='submit'>Submit</Button>
      </Form>
      </div>
    );
  };
  
  const validate = val => {
    const errors = {};
    if (!val.Name) {
      console.log('First Name is required');
      errors.Name = 'Required';
    }
    if (!val.email) {
      console.log('email is required');
      errors.email = 'Required';
    } else if (!/^.+@.+$/i.test(val.email)) {
      console.log('email is invalid');
      errors.email = 'Invalid email address';
    }
    if (!val.phoneNo) {
      errors.phoneNo = 'Required'
    } else if (isNaN(Number(val.age))) {
      errors.phoneNo = 'Must be a number'
    }
    return errors;
  };

  const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
    <div>
      <div className="control">
        <label className="field">{label}</label>
        <input className="input" {...input} placeholder={label} type={type}/>
        {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
      </div>
    </div>
  )
  export default reduxForm({
    form: 'simpleForm',
    validate,
  })(Example)
