import React from 'react';

import Form from "./users/form";
import ShowPost from './users/ShowPost'
import ShowPostSelector from './ShowPostSelector'
import Test from "./test"
import Home from "./Home"

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  useRouteMatch,
  useParams
} from "react-router-dom";

class App extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      post: {}
    }
  }

  onSubmit = (values) => {
    this.setState({post: values})
  }

  render() {
      return(
      //   <Router>
      //   <div>
      //     <nav>
      //     <ul >
      //       <li><Link to={'/'} className="nav-link"> Home </Link></li>
      //       <li><Link to={'/users/create'} className="nav-link">Create User</Link></li>
      //       <li><Link to={'/users/view'} className="nav-link">View User</Link></li>
      //     </ul>
      //     </nav>
      //     <hr />
      //     <Switch>
      //         <Route exact path='/' component={Home} />
      //         <Route path='/users/create' component={<Form onSubmit={this.onSubmit} />} />
      //         <Route path='/users/view' component={<ShowPost post={this.state.post} />} />
      //     </Switch>
      //   </div>
      // </Router>

        <React.Fragment>
          <Form onSubmit={this.onSubmit} />

          <br /> <br />
          <ShowPost post={this.state.post} />
          {/* <Test /> */}
          <ShowPostSelector />
          {/* <button onClick={}> </button> */}
          
        </React.Fragment>
      )
    }
  }
export default App
