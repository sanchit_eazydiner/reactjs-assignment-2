import React from 'react'
import { formValueSelector } from 'redux-form'
import { connect } from 'react-redux'

let Test = (props) =>
  <div>
    <h2>Submit</h2>
      <pre>
        {JSON.stringify(props.Name)}
      </pre>
  </div>

const mapStateToProps = state => ({
  Name: formValueSelector("simpleForm")(state, "name"),
  Gender: formValueSelector("simpleForm")(state, "gender"),
  Email: formValueSelector("simpleForm")(state, "email"),
  phone: formValueSelector("simpleForm")(state, "phoneNo"),
  file: formValueSelector("simpleForm")(state, "file"),
  checkbox: {C: formValueSelector("simpleForm")(state, "C"),Cpp : formValueSelector("simpleForm")(state, "C++")}
})

Test = connect(
  mapStateToProps
)(Test)

export default Test